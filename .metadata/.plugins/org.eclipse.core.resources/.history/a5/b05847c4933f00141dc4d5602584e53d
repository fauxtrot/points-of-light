// Copyright 2007-2014 metaio GmbH. All rights reserved.
package com.magenic.pol;

import java.io.File;
import java.io.IOException;

import android.app.ActionBar;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.Session;
import com.magenic.pol.R;
import com.metaio.sdk.ARViewActivity;
import com.metaio.sdk.MetaioDebug;
import com.metaio.sdk.jni.ESCREEN_ROTATION;
import com.metaio.sdk.jni.EVISUAL_SEARCH_STATE;
import com.metaio.sdk.jni.IGeometry;
import com.metaio.sdk.jni.IMetaioSDKAndroid;
import com.metaio.sdk.jni.IMetaioSDKCallback;
import com.metaio.sdk.jni.IVisualSearchCallback;
import com.metaio.sdk.jni.ImageStruct;
import com.metaio.sdk.jni.LLACoordinate;
import com.metaio.sdk.jni.Rotation;
import com.metaio.sdk.jni.StringVector;
import com.metaio.sdk.jni.TrackingValues;
import com.metaio.sdk.jni.TrackingValuesVector;
import com.metaio.sdk.jni.Vector3d;
import com.metaio.sdk.jni.VisualSearchResponseVector;
import com.metaio.tools.io.AssetsManager;

public class ARActivity extends ARViewActivity 
{

	private IGeometry mArrow;
	private boolean mArrowLoaded;
	private MetaioSDKCallbackHandler mSDKCallback;
	private VisualSearchCallbackHandler mVisualSearchCallback;
	private MediaPlayer mMplayer;
	
	private boolean isLoggedInFB;
	
	public void takeScreenShot(View view)
	{
		metaioSDK.requestScreenshot();   
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		mArrowLoaded = false;
		metaioSDK.enableBackgroundProcessing();
		this.mMplayer = MediaPlayer.create(this, R.raw.helms_audio);
		try {
			mMplayer.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		ActionBar bar = getActionBar();
//		bar.show();
		
		mSDKCallback = new MetaioSDKCallbackHandler();
		mVisualSearchCallback = new VisualSearchCallbackHandler();
		
		if (metaioSDK != null)
		{	
			metaioSDK.registerVisualSearchCallback(mVisualSearchCallback);
		}
	}

	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		mSDKCallback.delete();
		mSDKCallback = null;
		mVisualSearchCallback.delete();
		mVisualSearchCallback = null;
	}
	
	@Override
	protected int getGUILayout() 
	{
		// Attaching layout to the activity
		return R.layout.template; 
	}
	
	@Override
	protected void loadContents() 
	{
		try
		{
			Camera camera = IMetaioSDKAndroid.getCamera(this);
	        Camera.Parameters params = camera.getParameters();
	        params.setFocusMode("continuous-picture");
	        camera.setParameters(params);
			
			// Getting a file path for tracking configuration XML file
			String trackingConfigFile = AssetsManager.getAssetPath(getApplicationContext(), "Tracking.xml");
			
			// Assigning tracking configuration
			boolean result = metaioSDK.setTrackingConfiguration(trackingConfigFile);
			//boolean result = metaioSDK.setTrackingConfiguration("GPS");
			
			//metaioSDK.startInstantTracking("INSTANT_2D_GRAVITY_SLAM_EXTRAPOLATED", "", false);
			//MetaioDebug.log("Tracking data loaded: " + result); 
			//metaioSDK.requestCameraImage();
			final float scale = 36.f;
			final Rotation rotation = new Rotation(new Vector3d(90.f, 0.0f, 0.0f)); //(float)Math.PI/2
	        //final Vector3d translation = new Vector3d(0f, 1425.2f, -3196.9f);
			// Getting a file path for a 3D geometry
			final String arrowModel = AssetsManager.getAssetPath(getApplicationContext(), "final.zip");			
			if (arrowModel != null) 
			{
				
				// Loading 3D geometry
				mArrow = metaioSDK.createGeometry(arrowModel);
				mArrow.setAnimationSpeed(30.f);
				//mArrow.setVisible(false);
				if (mArrow != null) 
				{
					//mArrow.setTranslation(translation);
					// Set geometry properties
					mArrow.setScale(scale);
					mArrow.setRotation(rotation);
				}
				else
					MetaioDebug.log(Log.ERROR, "Error loading earth geometry: " + mArrow);
			}
			
			
			//TODO:: disable the button so this logic will work.
			Session session = Session.getActiveSession();
			if(session.isOpened())
			{
				Button b = (Button)findViewById(R.id.testButton);
				final boolean enabled = session.isOpened();
				b.setEnabled(enabled);
			}
			
			
//			final String earthOcclusionModel = AssetsManager.getAssetPath(getApplicationContext(), "Earth_Occlusion.zip");
//			if (earthOcclusionModel != null)
//			{
//				mEarthOcclusion = metaioSDK.createGeometry(earthOcclusionModel);
//				if (mEarthOcclusion != null)
//				{
//					mEarthOcclusion.setScale(scale);
//					mEarthOcclusion.setRotation(rotation);
//					mEarthOcclusion.setOcclusionMode(true);
//				}
//			}
//			else
//				MetaioDebug.log(Log.ERROR, "Error loading earth occlusion geometry: " + mEarthOcclusion);
//			
//			final String earthIndicatorsModel = AssetsManager.getAssetPath(getApplicationContext(), "EarthIndicators.zip");
//			if (earthIndicatorsModel != null)
//			{
//				mEarthIndicators = metaioSDK.createGeometry(earthIndicatorsModel);
//				if (mEarthIndicators != null)
//				{
//					mEarthIndicators.setScale(scale);
//					mEarthIndicators.setRotation(rotation);
//				}
//				else
//					MetaioDebug.log(Log.ERROR, "Error loading earth indicator geometry: " + mEarthIndicators);
//			}
			
		}       
		catch (Exception e)
		{
			MetaioDebug.log(Log.ERROR, "Failed to load content: " + e);
		}
	}
	
  
	@Override
	protected void onGeometryTouched(IGeometry geometry) 
	{
		MetaioDebug.log("Template.onGeometryTouched: " + geometry);
		StringVector sv = geometry.getAnimationNames();
		for(long i = 0; i < sv.size(); i++)
		{
			Log.d("ANIMATIONS LISTING:", sv.get((int)i));
			
			if(sv.get((int)i).trim().equals("Take 001") )
			{
				mMplayer.start();
				mArrow.startAnimation("Take 001", false);
			}
		}
		
//		if (geometry != mEarthOcclusion)
//		{
//			if (!mArrowLoaded)
//			{
//				mArrow.startAnimation("Open", false);
//				//mEarthIndicators.startAnimation("Grow", false);
//				mArrowLoaded = true;
//			}
//			else
//			{
//				mArrow.startAnimation("Close", false);
//				//mEarthIndicators.startAnimation("Shrink", false);
//				mArrowLoaded = false;
//			}
		//}

	}


	@Override
	protected IMetaioSDKCallback getMetaioSDKCallbackHandler() 
	{
		return mSDKCallback;
	}
	
	final class MetaioSDKCallbackHandler extends IMetaioSDKCallback 
	{
 
		@Override
		public void onSDKReady() 
		{
			MetaioDebug.log("The SDK is ready");
		}
		
		@Override
		public void onAnimationEnd(IGeometry geometry, String animationName) 
		{
			MetaioDebug.log("animation ended" + animationName);
		}
		
		@Override
		public void onMovieEnd(IGeometry geometry, String name)
		{
			MetaioDebug.log("movie ended" + name);
		}
		
		@Override
		public void onNewCameraFrame(ImageStruct cameraFrame)
		{	
			metaioSDK.setImage(cameraFrame);
			MetaioDebug.log("a new camera frame image is delivered" + cameraFrame.getTimestamp());
		}
		
		
		@Override 
		public void onCameraImageSaved(String filepath)
		{
			MetaioDebug.log("a new camera frame image is saved to" + filepath);
		}
		
		@Override
		public void onScreenshotImage(ImageStruct image)
		{
			Bitmap btmap = image.getBitmap();
			
			String file = MediaStore.Images.Media.insertImage(getContentResolver(), btmap, "TEST", "TEST");
			
			final String msg = "Image saved! Check out the lightbox...";
			
			Toast toast = Toast.makeText(getApplicationContext(), msg, 1500);
			toast.show();
			
			MetaioDebug.log("screenshot image is received" + image.getTimestamp());
		}
		
		@Override
		public void onScreenshotSaved(String filepath)
		{
			MetaioDebug.log("screenshot image is saved to" + filepath);
		}
		
		@Override
		public void onTrackingEvent(TrackingValuesVector trackingValues)
		{
			for (int i=0; i<trackingValues.size(); i++)
			{
				final TrackingValues v = trackingValues.get(i);
				if(v.getState().equals(com.metaio.sdk.jni.ETRACKING_STATE.ETS_FOUND))
				{
					if(!initializedTracking)
					{
						mMplayer.start();
						mArrow.startAnimation("Take 001", false);
						initializedTracking = true;
					}
					
//					if(!initializedTracking)
//					{
//						
//						MetaioDebug.log("Starting GPS Tracking.");
//						//boolean result = metaioSDK.setTrackingConfiguration("GPS");
//						LLACoordinate coords = mArrow.getTranslationLLA();
//												
//						//metaioSDK.startCamera(0);
////						mArrow.setLLALimitsEnabled(true);
////						mArrow.setTranslationLLA(coords);
//						MetaioDebug.log("Lat " + coords.getLatitude() + " |Long: " + coords.getLongitude() + " |ELEV " + coords.getAltitude());
////						
////						initializedTracking = true;
//						
					}
//				else if(v.getState().equals(com.metaio.sdk.jni.ETRACKING_STATE.ETS_LOST))
//				{
//					mArrow.pauseAnimation();
//					mMplayer.pause();
//				}
					
				MetaioDebug.log("Tracking state for COS "+ v.getCoordinateSystemID()+" is " + v.getState());
				}
				
			}
//		}

		@Override
		public void onInstantTrackingEvent(boolean success, String file)
		{
			if (success)
			{
				//metaioSDK.setTrackingConfiguration(file);
				MetaioDebug.log("Making the model visible.");
				mArrow.setVisible(true);
				MetaioDebug.log("Instant tracking is successful");
			}
		}
		
		private boolean initializedTracking = false;
	}
	
	final class VisualSearchCallbackHandler extends IVisualSearchCallback
	{

		@Override
		public void onVisualSearchResult(VisualSearchResponseVector response, int errorCode)
		{
			if (errorCode == 0)
			{
				MetaioDebug.log("Visual search is successful");
			}
		}

		@Override
		public void onVisualSearchStatusChanged(EVISUAL_SEARCH_STATE state) 
		{
			MetaioDebug.log("The current visual search state is: " + state);
		}


	}
	
}
