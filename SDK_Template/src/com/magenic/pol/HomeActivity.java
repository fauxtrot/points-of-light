package com.magenic.pol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class HomeActivity extends PointsOfLightActivity {

	@Override
	protected void onCreate(Bundle savedInstatnceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstatnceState);
		setContentView(R.layout.activity_home);
		
		List<DevCredit> list = new ArrayList<DevCredit>();
		list.add(new DevCredit("Dalon Huntington", "Content / Video / 3D Modeling"));
		list.add(new DevCredit("Nathan Dupuis", "User Experience / Vision / 'ImagiNathan'"));
		list.add(new DevCredit("Danyon Huntington", "Kinect MoCap Tech / Doughnut king"));
		list.add(new DevCredit("Peter Simard", "as Edgar J. Helms"));
		list.add(new DevCredit("Todd Richardson", "Architect / Code Monkey"));
		
		DevCreditArrayAdapter adapter = new DevCreditArrayAdapter(this, R.id.credList, list);
		
		ListView listView = (ListView)findViewById(R.id.credList);
		listView.setAdapter(adapter);
	}
	
	public class DevCreditArrayAdapter extends ArrayAdapter<DevCredit>
	{
		private final List<DevCredit> list;
		
		Context mctx;		
		
		
		public DevCreditArrayAdapter(Context context, int resource, List<DevCredit> objects) {
			super(context, resource, objects);
			
			this.mctx = context;
			
			list = objects;
		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			android.view.LayoutInflater  inflater = 
					(android.view.LayoutInflater) mctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			View rowView = inflater.inflate(R.layout.developer_credit_partial, parent, false);
			TextView name = (TextView)rowView.findViewById(R.id.credName);
			name.setText(((DevCredit)this.getItem(position)).getDevName());
			TextView role = (TextView)rowView.findViewById(R.id.credRole);
			role.setText(((DevCredit)this.getItem(position)).getDevRole());
			
			return rowView;
		}
		

		

	}
	
	public class DevCredit
	{
		private String mDevName;
		private String mDevRole;
		
		public DevCredit(String devName, String devRole)
		{
			this.mDevName = devName;
			this.mDevRole = devRole;
		}
		
		public String getDevName()
		{
			return mDevName;
		}
		
		public String getDevRole()
		{
			return mDevRole;
		}
	}
}
