package com.magenic.pol;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

public class PointsOfLightActivity extends Activity {

	
	private UiLifecycleHelper uiHelper;
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
	        if (state.isOpened()) { 
	            isLoggedIn = true;
	        } else if (state.isClosed()) {
	            isLoggedIn = false;
	        }
	    }   
	};
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_home:
	            openHome();
	            return true;
	        case R.id.action_explore:
	            openExplore();
	            return true;
	        case R.id.action_map:
	        	openMap();
	        	return true;
	        case R.id.action_pol:
	        	openPol();
	        	return true;
	        case R.id.action_profile:
	        	openProfile();
	        	return true;
	        case android.R.id.home:
	        	onBackPressed();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void openProfile()
	{
		Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
		startActivity(i);
	}
	
	private void openPol()
	{
		Intent i = new Intent(getApplicationContext(), ARActivity.class);
		startActivity(i);
	}
	
	private void openHome()
	{
		if(this.getClass() == HomeActivity.class)
		{
			return;
		}
		Intent i = new Intent(getApplicationContext(), HomeActivity.class);
		startActivity(i);
	}
	
	private void openExplore()
	{
		if(this.getClass() == ExploreActivity.class)
		{
			return;
		}
		Intent i = new Intent(getApplicationContext(), ExploreActivity.class);
		startActivity(i);
	}
	
	private void openMap()
	{
		if(this.getClass() == HomeActivity.class)
		{
			return;
		}
		onTodo();
	}
	
	private void onTodo()
	{
		
		Toast toast = Toast.makeText(getApplicationContext(), "Coming Soon", 3000);
		toast.show();
	}
	
	private boolean isLoggedIn = false;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.points_of_light_actionmenu, menu);
	    getActionBar().setDisplayShowHomeEnabled(true);
	    getActionBar().setHomeButtonEnabled(true);
	    getActionBar().setDisplayHomeAsUpEnabled(true);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
	  super.onSaveInstanceState(savedInstanceState);
	  
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		super.onActivityResult(arg0, arg1, arg2);
		uiHelper.onActivityResult(arg0, arg1, arg2);
		
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	protected void onCreate(Bundle savedInstatnceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstatnceState);
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstatnceState);
//		setContentView(R.layout.activity_polmain);
//		populateActionBar();
	}	
	
	private void populateActionBar()
	{
		 final ActionBar actionBar = getActionBar();
		    
		    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		    
		    ActionBar.TabListener tabListener = new ActionBar.TabListener() {
				
				@Override
				public void onTabUnselected(Tab tab, FragmentTransaction ft) {
					//hide tab
					if(tab.getTag().equals("Home"))
					{
						
						openHome();
					}
					if(tab.getTag().equals("Explore"))
					{
						openExplore();
					}
					if(tab.getTag().equals("POL"))
					{
						openPol();
					}
					if(tab.getTag().equals("Map"))
					{
						openMap();
					}
					if(tab.getTag().equals("Profile"))
					{
						openProfile();
					}
				}
				
				@Override
				public void onTabSelected(Tab tab, FragmentTransaction ft) {
					// TODO Show Tab
					
				}
				
				@Override
				public void onTabReselected(Tab tab, FragmentTransaction ft) {
					// TODO ??
					
				}
			};
			final Tab homeTab = actionBar.newTab()
					.setText("Home")
					.setTabListener(tabListener);
			Tab exploreTab = actionBar.newTab()
					.setText("Explore")
					.setTabListener(tabListener);
			Tab polTab = actionBar.newTab()
					.setText("POL")
					.setTabListener(tabListener);
			Tab mapTab = actionBar.newTab()
					.setText("Map")
					.setTabListener(tabListener);
			Tab profileTab = actionBar.newTab()
					.setText("Profile")
					.setTabListener(tabListener);
		    actionBar.addTab(homeTab);
		    actionBar.addTab(exploreTab);
		    actionBar.addTab(polTab);
		    actionBar.addTab(mapTab);
		    actionBar.addTab(profileTab);
	}
	
//	private void populateTabs()
//	{
//		TabSpec homeTb = mTabHost.newTabSpec("Home");
//		TabSpec exploreTb = mTabHost.newTabSpec("Explore");
//		TabSpec polTb = mTabHost.newTabSpec("POL");
//		TabSpec mapTb = mTabHost.newTabSpec("Map");
//		TabSpec profileTb = mTabHost.newTabSpec("Profile");
//	}
	
	
}
