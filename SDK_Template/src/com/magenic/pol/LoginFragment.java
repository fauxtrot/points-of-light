package com.magenic.pol;

import java.io.IOException;

import com.magenic.pol.BuildConfig;
import com.magenic.pol.R;
import com.metaio.sdk.MetaioDebug;
import com.metaio.tools.io.AssetsManager;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;

public class LoginFragment extends Fragment {

	/**
	 * Task that will extract all the assets
	 */
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
	
		return inflater.inflate(R.layout.login_activity, container, false);
		
	}
	
	public void continueToMetaio(View view)
	{
		Intent intent = new Intent(getActivity(), HomeActivity.class);
		startActivity(intent);
	}
	
	
	
}
