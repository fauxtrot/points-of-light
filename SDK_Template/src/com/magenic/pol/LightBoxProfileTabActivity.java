package com.magenic.pol;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ViewFlipper;

public class LightBoxProfileTabActivity extends Fragment {

	GridView mGridView;
	ViewFlipper mViewFlipper;
	ProgressBar mProgress;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View retval = inflater.inflate(R.layout.profile_fragment_lightbox, container, false);
		
		mGridView = (GridView)retval.findViewById(R.id.gridView1);
		mViewFlipper = (ViewFlipper)retval.findViewById(R.id.viewFlipper1);
		mProgress = (ProgressBar)retval.findViewById(R.id.progressBar1);
		
		AsyncTask<Void, Void, List<Bitmap>> task = new AsyncTask<Void, Void, List<Bitmap>>() {

			@Override
			protected List<Bitmap> doInBackground(Void... params) {
				return getPOLImages();
			}

			@Override
			protected void onPostExecute(List<Bitmap> result) {
				
				LightBoxImageAdapter lbia = new LightBoxImageAdapter(getActivity(), result);
				
				mGridView.setAdapter(lbia);
				
				mViewFlipper.showNext();
			}
		};
		
		task.execute();
		
		return retval;
	}

	public List<Bitmap> getPOLImages()
	{
		String[] projection = new String[]{
				MediaStore.Images.Media._ID,
				MediaStore.Images.Media.TITLE,
				MediaStore.Images.Media.DATA
		};
		List<Bitmap> retval = new ArrayList<Bitmap>();
		Cursor cur = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, MediaStore.Images.Media.TITLE + " like 'POL_%'", null, null);//MediaStore.Images.Media.query()
		int dataColumnIndex = cur.getColumnIndex(MediaStore.Images.Media.DATA);
		while(cur.moveToNext())
		{
			 try     
		        {

		            final int THUMBNAIL_SIZE = 256;

		            String path = cur.getString(dataColumnIndex);
					//Bitmap bm = BitmapFactory.decodeFile(path);
					
					Log.d("GAL_ENTRY", path);
		            
		            FileInputStream fis = new FileInputStream(path);
		            Bitmap imageBitmap = BitmapFactory.decodeStream(fis);

		            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, THUMBNAIL_SIZE, THUMBNAIL_SIZE, false);

		            ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		            
		            retval.add(imageBitmap);

		        }
		        catch(Exception ex) {

		        }
			
			
			
		}
		return retval;
	}
	
	public class LightBoxImageAdapter extends BaseAdapter
	{
		  private Context mContext;
		  private List<Bitmap> mPolImages;
		  
		  public LightBoxImageAdapter(Context context, List<Bitmap> images)
		  {
			  mContext = context;
			  mPolImages = images;
		  }
		  
		  public int getCount() {
		        return mPolImages.size();
		    }

		    public Object getItem(int position) {
		        return null;
		    }

		    public long getItemId(int position) {
		        return 0;
		    }

		    // create a new ImageView for each item referenced by the Adapter
		    public View getView(int position, View convertView, ViewGroup parent) {
		        ImageView imageView;
		        if (convertView == null) {  // if it's not recycled, initialize some attributes
		            imageView = new ImageView(mContext);
		            imageView.setLayoutParams(new GridView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		            imageView.setPadding(8, 8, 8, 8);
		        } else {
		            imageView = (ImageView) convertView;
		        }

		        imageView.setImageBitmap(mPolImages.get(position));
		        return imageView;
		    }
		
	}
}
