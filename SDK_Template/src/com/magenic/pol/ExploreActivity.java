package com.magenic.pol; 

import java.lang.reflect.Field;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ExploreActivity extends PointsOfLightActivity {
	
	GridView gvMedallion;
	
	@Override
	protected void onCreate(Bundle savedInstatnceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstatnceState);
		setContentView(R.layout.activity_explore);
		gvMedallion = (GridView)findViewById(R.id.medallionGrid);
		gvMedallion.setAdapter(new ImageAdapter(this));
		
	}
	
	
	private ImageView getImageViewByName(String identifier)
	{
		Resources r = getResources();
		Class res = R.drawable.class;
		Field field = null;
		try {
			field = res.getField(identifier);
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int drawable = 0;
		try {
			drawable = field.getInt(null);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		ImageView retval = new ImageView(this);
		retval.setLayoutParams(params);
		retval.setImageBitmap(BitmapFactory.decodeResource(r, drawable));
		return retval;
	}
	
	public class ImageAdapter extends BaseAdapter {
	    private Context mContext;

	    public ImageAdapter(Context c) {
	        mContext = c;
	    }

	    public int getCount() {
	        return mThumbIds.length;
	    }

	    public Object getItem(int position) {
	        return null;
	    }

	    public long getItemId(int position) {
	        return 0;
	    }

	    // create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ImageView imageView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
	            imageView.setLayoutParams(new GridView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
	            imageView.setPadding(8, 8, 8, 8);
	        } else {
	            imageView = (ImageView) convertView;
	        }

	        imageView.setImageResource(mThumbIds[position]);
	        return imageView;
	    }

	    // references to our images
	    private Integer[] mThumbIds = {
	           R.drawable.md_booth,
	           R.drawable.md_e_allen,
	           R.drawable.md_e_coulter,
	           R.drawable.md_e_helms,
	           R.drawable.md_j_adams,
	           R.drawable.md_j_muir
	    };
	}
}
