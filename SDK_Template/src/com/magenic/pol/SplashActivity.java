package com.magenic.pol;

import java.io.IOException;
import java.util.Date;

import com.metaio.sdk.MetaioDebug;
import com.metaio.tools.io.AssetsManager;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;

public class SplashActivity extends Activity{

	private AssetsExtracter mTask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.splash_view);
	MetaioDebug.enableLogging(BuildConfig.DEBUG);
		
		// extract all the assets
		mTask = new AssetsExtracter();
		mTask.execute(0);
		
		
	}
	
	/**
	 * This task extracts all the assets to an external or internal location
	 * to make them accessible to metaio SDK
	 */
	private class AssetsExtracter extends AsyncTask<Integer, Integer, Boolean>
	{
		private final long SPLASH_TIME_OUT = 3000;
		private Date start;
		private Date finish;
		@Override
		protected void onPreExecute() 
		{
			start = new Date();
		}
		
		@Override
		protected Boolean doInBackground(Integer... params) 
		{
			try 
			{
				// Extract all assets and overwrite existing files if debug build
				AssetsManager.extractAllAssets(getApplicationContext(), BuildConfig.DEBUG);
			} 
			catch (IOException e) 
			{
				MetaioDebug.log(Log.ERROR, "Error extracting assets: "+e.getMessage());
				MetaioDebug.printStackTrace(Log.ERROR, e);
				return false;
			}
			
			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean result) 
		{
			finish = new Date();
			
			long mil_elapsed = (finish.getTime() - start.getTime());
			
			long timeLeft = SPLASH_TIME_OUT - mil_elapsed;
			
			if(timeLeft > 0)
			{
				new Handler().postDelayed(new Runnable(){
					
					public void run()
					{
						doIntent();
					}
				}, timeLeft);
			}
			else
			{
				doIntent();
			}
	    }
		
		private void doIntent()
		{
			
			Intent intent = new Intent(getApplicationContext(), MainActivity.class);
			startActivity(intent);
			
			finish();
		}
		
	}
	
}
