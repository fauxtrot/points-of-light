// Copyright 2007-2014 metaio GmbH. All rights reserved.
package com.magenic.pol;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.magenic.pol.PointsOfLightAR.UserInfo;

public class MainActivity extends FragmentActivity
{

	
	private boolean isLoggedIn = false; // by default assume not logged in

	private UiLifecycleHelper uiHelper;
	
	private Session.StatusCallback callback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
	        if (state.isOpened()) { 
	            isLoggedIn = true;
	        } else if (state.isClosed()) {
	            isLoggedIn = false;
	        }
	    }   
	};

	public void continueToMetaio(View view)
	{
		loginFragment.continueToMetaio(view);
	}
	
	public void startTutorial(View view)
	{
		Intent i = new Intent(this, TutorialActivity.class);
		startActivity(i);
	}
	
	private LoginFragment loginFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
	
//		try {
//		       PackageInfo info = getPackageManager().getPackageInfo(
//		                "com.magenic.pol", 
//		                PackageManager.GET_SIGNATURES);
//		        for (Signature signature : info.signatures) {
//		            MessageDigest md = MessageDigest.getInstance("SHA");
//		            md.update(signature.toByteArray());
//		            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//		            }
//		    } catch (NameNotFoundException e) {
//
//		    } catch (NoSuchAlgorithmException e) {
//
//		    }
//		
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
		
		Session session = Session.openActiveSessionFromCache(getApplicationContext());
		
		if (savedInstanceState == null) {
	        // Add the fragment on initial activity setup
	        loginFragment = new LoginFragment();
	        getSupportFragmentManager()
	        .beginTransaction()
	        .add(android.R.id.content, loginFragment)
	        .commit();
	    } else {
	        // Or set the fragment from restored state info
	    	loginFragment = (LoginFragment) getSupportFragmentManager()
	        .findFragmentById(android.R.id.content);
	    }
		
		setContentView(R.layout.main);
		
		try {
          
				//File file = new File(getFilesDir() + "/FB_Image.png");
            	if(PointsOfLightAR.getInstance().getcurrentUser().getfbUserPic() == null)
            	{
            		return;
            	}
				
				Bitmap bitmap = PointsOfLightAR.getInstance().getcurrentUser().getfbUserPic();
																						
            	LayoutInflater inflater = getLayoutInflater();
            	View view = inflater.inflate(R.layout.toast_welcome_fbuser, (ViewGroup)findViewById(R.id.fbtoast_welcomeback));
            	ImageView v = (ImageView)view.findViewById(R.id.fbtoast_userImage);
            	v.setImageBitmap(bitmap); 
            	
            	Toast toast = new Toast(getApplicationContext());
            	
            	toast.setView(view);
            	
            	toast.show();
        

            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
				//String url = stringURL = "https://graph.facebook.com/"+URLEncoder.encode(DataStorage.getFB_USER_ID(),"UTF-8")+"?fields="+URLEncoder.encode("picture","UTF-8");
	}
		


	@Override
	protected void onDestroy() {
		uiHelper.onDestroy();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		uiHelper.onActivityResult(arg0, arg1, arg2);
		super.onActivityResult(arg0, arg1, arg2);
	}

	@Override
	protected void onPause() {
		uiHelper.onPause();
		super.onPause();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		uiHelper.onSaveInstanceState(outState);
		super.onSaveInstanceState(outState);
	}
}

