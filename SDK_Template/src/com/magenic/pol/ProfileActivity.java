package com.magenic.pol;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TabHost.TabSpec;

import com.magenic.pol.MainActivity;

public class ProfileActivity extends FragmentActivity {

	private FragmentTabHost mTabHost; 
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
		setupTabs();
		setupTop();
	}
		
	private void setupTop(){
		ImageView iv = (ImageView)findViewById(R.id.profile_image_fbuser);
		TextView tv = (TextView)findViewById(R.id.profile_name);
		PointsOfLightAR.UserInfo userInfo = PointsOfLightAR.getInstance().getcurrentUser();
		//Image
		Bitmap bm = userInfo.getfbUserPic();
		if(bm == null)
		{
			return;
		}
		iv.setImageBitmap(bm);
		//Name
		tv.setText(userInfo.getfbUsername());
		
	}
	
	private void setupTabs()
	{
		TabSpec passport = mTabHost.newTabSpec("Passport");
		
		TabSpec lightbox = mTabHost.newTabSpec("Lightbox");
		
		TabSpec awards = mTabHost.newTabSpec("Awards");

		passport.setIndicator("Passport");
		lightbox.setIndicator("Lightbox");
		awards.setIndicator("Awards");
				
		mTabHost.clearAllTabs();
		
		mTabHost.addTab(passport, PassportProfileTabActivity.class, null);
		mTabHost.addTab(lightbox, LightBoxProfileTabActivity.class, null);
		mTabHost.addTab(awards, AwardsProfileTabActivity.class, null);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_home:
	            openHome();
	            return true;
	        case R.id.action_explore:
	            openExplore();
	            return true;
	        case R.id.action_map:
	        	openMap();
	        	return true;
	        case R.id.action_pol:
	        	openPol();
	        	return true;
	        case R.id.action_profile:
	        	openProfile();
	        	return true;
	        case android.R.id.home:
	        	onBackPressed();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void openProfile()
	{
		return;
	}
	
	private void openPol()
	{
		Intent i = new Intent(getApplicationContext(), ARActivity.class);
		startActivity(i);
	}
	
	private void openHome()
	{
		
		Intent i = new Intent(getApplicationContext(), HomeActivity.class);
		startActivity(i);
	}
	
	private void openExplore()
	{
		
		Intent i = new Intent(getApplicationContext(), ExploreActivity.class);
		startActivity(i);
	}
	
	private void openMap()
	{
		
		onTodo();
	}
	
	private void onTodo()
	{
		
		Toast toast = Toast.makeText(getApplicationContext(), "Coming Soon", 3000);
		toast.show();
	}
	
	private boolean isLoggedIn = false;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.points_of_light_actionmenu, menu);
	    getActionBar().setDisplayShowHomeEnabled(true);
	    getActionBar().setHomeButtonEnabled(true);
	    getActionBar().setDisplayHomeAsUpEnabled(true);
	    return super.onCreateOptionsMenu(menu);
	}
	
	
	
}
