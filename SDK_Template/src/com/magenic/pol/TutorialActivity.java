package com.magenic.pol;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

public class TutorialActivity extends Activity {
	
	private static String CURRENT_STEP_KEY = "CURRENT_STEP";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.tutorial_step1);
	}
	
	public void gotoStep1(View view)
	{
		setContentView(R.layout.tutorial_step1);
	}
	
	public void gotoStep2(View view)
	{
		setContentView(R.layout.tutorial_step2);
	}
	
	public void gotoStep3(View view)
	{
		setContentView(R.layout.tutorial_step3);
	}
	
	public void finishTutorial(View view)
	{
		Intent i = new Intent(this, ARActivity.class);
		startActivity(i);
		finish();
	}
	
}
