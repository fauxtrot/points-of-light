package com.magenic.pol;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class PointsOfLightAR extends Application {
	
	private static PointsOfLightAR instance;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		//load preferences from SharePreferences
		
		instance = this;
		instance.currentUser = new PointsOfLightAR.UserInfo();
		loadUserInfo();
	}

	private void loadUserInfo() {
		Context context = getApplicationContext();
		SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.pol_preference_keyfile), context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		Session session = Session.openActiveSessionFromCache(context);
		if(session != null && session.isOpened())
		{
			final Handler h = new Handler(getApplicationContext().getMainLooper());
			if(session != null && session.isOpened())
			{
				Request.newMeRequest(session, new Request.GraphUserCallback() {
	                @Override
	                public void onCompleted(GraphUser user, Response response) {
	                    if (user != null) {
	                    	final GraphUser mUser = user;
	                    	instance.currentUser.setfbUsername(user.getName());
	                    	new Thread(new Runnable(){

	                    		                   		
								@Override
								public void run() {
									   FileOutputStream fos = null;
			                            Bitmap bitmap = null;
			                            try
			                            {
			                            	File file = new File(getFilesDir() + "/FB_Image.png");
			                            	if(file.exists())
			                            	{
			                            		bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
			                            		instance.currentUser.setfbUserPic(bitmap);
			                            		return;
			                            	}
			                            		
			                            	fos =  openFileOutput("FB_Image.png", Context.MODE_PRIVATE);
			                            	
			                            	URL imgUrl = new URL("https://graph.facebook.com/"
				                                     + mUser.getId() + "/picture?type=large");

				                            InputStream in = (InputStream) imgUrl.getContent();
				                            bitmap = BitmapFactory.decodeStream(in);
			                            	
			                                bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
			                                instance.currentUser.setfbUserPic(bitmap);
			                            }
			                            catch (Exception ex)
			                            {
			                            	ex.printStackTrace();
			                            }
			                            finally
			                            {
			                            	if(fos != null)
			                            	{
			                            		try {
													fos.close();
												} catch (IOException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
			                            	}
			                            	
			                            }
								}}).start();
	                    }
	                }
	            }).executeAsync();
			}
		}
	}
	
	private UserInfo currentUser;
	
	public UserInfo getcurrentUser()
	{
		return currentUser;
	}
	
	public static PointsOfLightAR getInstance()
	{
		return instance;
	}
	
	public class UserInfo
	{
		private Bitmap fbUserPic;
		
		public Bitmap getfbUserPic()
		{
			return fbUserPic;
		}
		
		public void setfbUserPic(Bitmap pic)
		{
			fbUserPic = pic;
		}
		
		private String fbUsername;
		
		public String getfbUsername()
		{
			return fbUsername;
		}
		
		public void setfbUsername(String username)
		{
			fbUsername = username;
		}
	}
}
